#!/usr/bin/env python3
import sys, time, struct, os, argparse
import itertools
from collections import deque

import numpy as np
import png

import theano
import theano.tensor as T

import math

class NNLayer(object):
    ''' represents a layer in the nerual network '''
    # TODO: __slots__

    def theta(self):
        return [ self.W, self.b, self.b_prime ]

    def draw_filters(self, row_w, row_h):
        W = self.W.get_value().T

        if row_w and row_h:
            in_width, in_height = row_h, row_w
            out_width = math.ceil(W.shape[0] ** 0.5)
            out_height = math.ceil(W.shape[0] / out_width)
        else:
            out_width, in_width   = map(lambda x: math.ceil(x ** 0.5), W.shape)
            out_height, in_height = map(lambda xy: math.ceil(xy[0] / xy[1]), zip(W.shape, [out_width, in_width]))

        image = np.zeros((in_width * out_width + out_width - 1, in_height * out_height + out_height - 1), theano.config.floatX)

        for i, row in enumerate(W):
            row -= row.min()
            row /= row.max()

            x, y = (i % out_width) * (in_width + 1), (i // out_width) * (in_height + 1)
            image[x:x+in_width, y:y+in_height] = np.reshape(row, (in_width, in_height))

        return (image * 255).astype(np.uint8)

    def dump_filters(self, name, row_w, row_h):
        with open(os.path.join('output', name), 'wb') as f:
            data = self.draw_filters(row_w, row_h)
            writer = png.Writer(data.shape[1], data.shape[0], greyscale=True)
            writer.write(f, data)

    def __init__(self, args, rng, i):
        input_layer_size = args.dimensions if i == 0 else args.layer_sizes[i-1]
        init = 4 * np.sqrt(6.0 / (input_layer_size + args.layer_sizes[i]))
        initial = np.asarray(rng.uniform(low=-init, high=init, size=(input_layer_size, args.layer_sizes[i])), dtype=theano.config.floatX)

        # theano RNG
        rng_theano = theano.tensor.shared_randomstreams.RandomStreams(1)

        # weights and bias for this layer, we store it transposed so it's easier to dot product with the training examples
        self.W = W = theano.shared(initial)
        self.b = b = theano.shared(np.zeros(args.layer_sizes[i], dtype=theano.config.floatX))

        # dummy bias for autoencoder
        self.b_prime = b_prime = theano.shared(np.zeros(input_layer_size, dtype=theano.config.floatX))

        # pretraining cost is computed from reconstruction error, x = minibatch, y = output of hidden layer, z = output of reconstruction
        self.x = x = T.matrix('x_layer_' + str(i))
        self.x_tilde = x_tilde = rng_theano.binomial(size=(x.shape[0], x.shape[1]), n=1,  p=(1 - args.corruption_level), dtype=theano.config.floatX) * x
        self.y = y = T.nnet.sigmoid(T.dot(x_tilde, W) + b)
        self.z = z = T.nnet.sigmoid(T.dot(y, W.T) + b_prime)

        # compute average reconstruction cost
        self.reconstruction_cost_vector = reconstruction_cost_vector = T.sum(T.nnet.binary_crossentropy(z, x), axis=1)
        self.reconstruction_cost = reconstruction_cost = T.mean(reconstruction_cost_vector)
        self.gradients = gradients = T.grad(reconstruction_cost, self.theta())

        self.pretrain = theano.function([x], [y, reconstruction_cost], updates=[ (param, param - args.learning_rate * grad) for param, grad in zip(self.theta(), gradients) ])
        self.test_reconstruction = theano.function([x], [y, reconstruction_cost_vector])

        # index variable for merge and increment
        self.idx = T.ivector('idx')

def build_model(args):
    rng = np.random.RandomState(1)

    # parameters of the hidden layers
    layers = [ NNLayer(args, rng, i) for i in range(len(args.layer_sizes)) ]

    def pretrain_model(batch_x):
        costs = np.empty(len(layers))
        prev_activations = batch_x

        for i, nnlayer in enumerate(layers):
            for j in range(args.iterations):
                activations, cost = nnlayer.pretrain(prev_activations)

                if j == args.iterations - 1:
                    prev_activations = activations
                    costs[i] = cost

        return costs

    def test_model_reconstruction(batch_x):
        costs = np.zeros(len(batch_x))
        prev_activations = batch_x

        for i, nnlayer in enumerate(layers):
            activations, layer_cost = nnlayer.test_reconstruction(prev_activations)

            prev_activations = activations
            costs += layer_cost

        return costs

    # set up supervised training
    x = T.matrix('x')
    y = T.ivector('y')

    x2 = x
    params = []
    for nnlayer in layers:
        x2 = T.nnet.sigmoid(T.dot(x2, nnlayer.W) + nnlayer.b)
        params += [ nnlayer.W, nnlayer.b ]

    # attach log regression layer
    softmax_W = theano.shared(np.zeros((args.layer_sizes[-1], args.outputs), dtype=theano.config.floatX))
    softmax_b = theano.shared(np.zeros(args.outputs, dtype=theano.config.floatX))
    softmax_p_y_given_x = T.nnet.softmax(T.dot(x2, softmax_W) + softmax_b)
    softmax_theta = [ softmax_W, softmax_b ]
    softmax_result = T.argmax(softmax_p_y_given_x, axis=1)

    params += softmax_theta

    cost_matrix = -T.log(softmax_p_y_given_x)[T.arange(y.shape[0]), y]
    overall_cost = T.mean(cost_matrix)
    overall_optimise = theano.function([x, y], overall_cost, updates=[ (param, param - args.learning_rate_fine * grad) for param, grad in zip(params, T.grad(overall_cost, params)) ])
    get_errors = theano.function([x, y], T.mean(T.neq(softmax_result, y)))

    def train_model(batch_x, batch_y):
        for _ in range(args.iterations):
            overall_optimise(batch_x, batch_y)

    def test_model(batch_x, batch_y):
        return get_errors(batch_x, batch_y)

    # set up for scoring merges
    m = T.matrix('m')
    m_dists, _ = theano.map(lambda v: T.sqrt(T.dot(v, v.T)), m)
    m_cosine = (T.dot(m, m.T) / m_dists) / m_dists.dimshuffle(0, 'x')
    m_ranks = T.argsort((m_cosine - T.tri(m.shape[0]) * np.finfo(theano.config.floatX).max).flatten())[(m.shape[0] * (m.shape[0] + 1)) // 2:]

    score_merges = theano.function([m], m_ranks)

    # set up layered merge-increment, building a cost function
    mi_cost = overall_cost
    mi_updates = []
    for i, nnlayer in enumerate(layers):
        mi_cost += args.lam * nnlayer.reconstruction_cost

    for i, nnlayer in enumerate(layers):
        mi_updates += [ (nnlayer.W, T.inc_subtensor(nnlayer.W[:,nnlayer.idx], - args.learning_rate * T.grad(mi_cost, nnlayer.W)[:,nnlayer.idx].T)) ]
        mi_updates += [ (nnlayer.b, T.inc_subtensor(nnlayer.b[nnlayer.idx],   - args.learning_rate * T.grad(mi_cost, nnlayer.b)[nnlayer.idx]))     ]
        mi_updates += [ (nnlayer.b_prime, - args.learning_rate * T.grad(mi_cost, nnlayer.b_prime)) ]

    mi_updates += [ (param, param - args.learning_rate * grad) for param, grad in zip(softmax_theta, T.grad(mi_cost, softmax_theta)) ]
    mi_givens = { }
    prev_x = x
    for nnlayer in layers:
        mi_givens[nnlayer.x] = prev_x
        prev_x = T.nnet.sigmoid(T.dot(prev_x, nnlayer.W) + nnlayer.b)

    mi_train = theano.function([x, y] + [ layer.idx for layer in layers ], None, givens=mi_givens, updates=mi_updates, on_unused_input='warn')

    # optimise the new feature weights
    def merge_model(batch_x, batch_y, merge_count, inc_count):
        if not merge_count or not inc_count:
            return

        update_slots = []
        prev_dimensions = args.dimensions

        for i, nnlayer in enumerate(layers):
            used = set()
            used_slots, empty_slots = [], []
            layer_weights = nnlayer.W.get_value().T.copy()
            layer_bias = nnlayer.b.get_value().copy()

            for index in score_merges(layer_weights):
                if len(empty_slots) == merge_count: break
                x_i, y_i = index % layer_weights.shape[0], index // layer_weights.shape[0]

                if x_i not in used and y_i not in used:
                    # merge x_i with y_i
                    layer_weights[x_i] = (layer_weights[x_i] + layer_weights[y_i]) / 2
                    layer_bias[x_i] = (layer_bias[x_i] + layer_bias[y_i]) / 2

                    used.update([x_i, y_i])
                    used_slots.append(x_i)
                    empty_slots.append(y_i)

            # will need to add more space for new features
            new_layer_weights = np.zeros((layer_weights.shape[0] + inc_count - len(empty_slots), prev_dimensions), dtype=theano.config.floatX)
            new_layer_weights[:layer_weights.shape[0], :layer_weights.shape[1]] = layer_weights
            layer_bias.resize(layer_bias.shape[0] + inc_count - len(empty_slots))

            layer_bias_prime = nnlayer.b_prime.get_value().copy()
            layer_bias_prime.resize(prev_dimensions)

            prev_dimensions = new_layer_weights.shape[0]

            if inc_count > len(empty_slots):
                update_slots.append(used_slots + empty_slots + list(range(layer_weights.shape[0], layer_weights.shape[0] + inc_count - len(empty_slots))))
            else:
                update_slots.append(used_slots + empty_slots[:inc_count])

            # set the new data
            nnlayer.W.set_value(new_layer_weights.T)
            nnlayer.b.set_value(layer_bias)
            nnlayer.b_prime.set_value(layer_bias_prime)

        # update log layer weight matrix size
        log_layer_weights = softmax_W.get_value().copy()
        log_layer_weights.resize((prev_dimensions, args.outputs))
        softmax_W.set_value(log_layer_weights)

        # train
        total_batches = batch_y.shape[0] // args.pretraining_batch_size
        #def window_slots(i):
            #idx = i / total_batches
            #new_slots = []
            #for slots in update_slots:
                #slot_batch_size = math.ceil(len(xs) / total_batches)
                #new_slots.append(slots[idx * slot_batch_size : (idx + 1) * slot_batch_size)

        for i in range(total_batches):
            index = i * args.pretraining_batch_size
            for _ in range(args.iterations):
                mi_train(*([batch_x[index : index + args.pretraining_batch_size], batch_y[index : index + args.pretraining_batch_size]] + update_slots))
                #mi_train(*([batch_x[index : index + args.pretraining_batch_size], batch_y[index : index + args.pretraining_batch_size]] + window_slots(i)))

    return (pretrain_model, train_model, test_model, test_model_reconstruction, merge_model, layers)

def main():
    parser = argparse.ArgumentParser(description='Extract features')
    parser.add_argument('-b', '--batch-size', dest='batch_size', type=int, default=1000, help='Batch size')
    parser.add_argument('-bp', '--pretraining-batch-size', dest='pretraining_batch_size', type=int, default=20, help='Pretraining batch size')
    parser.add_argument('-p', '--pretraining-size', dest='pretrain_size', type=int, default=12000, help='How many training examples are used to pretrain the model')
    parser.add_argument('-i', '--iterations', dest='iterations', type=int, default=15, help='Number of training iterations per minibatch')
    parser.add_argument('-lr', '--learning-rate', dest='learning_rate', type=float, default=0.1)
    parser.add_argument('-lrf', '--learning-rate-fine', dest='learning_rate_fine', type=float, default=0.01)
    parser.add_argument('-d', '--dimensions', dest='dimensions', type=int, default=28*28, help='Number of dimensions')
    parser.add_argument('-c', '--corruption-level', dest='corruption_level', type=float, default=0.3)
    parser.add_argument('-o', '--outputs', dest='outputs', type=int, default=10, help='Number of outputs')
    parser.add_argument('-ls', '--hidden-layer-sizes', nargs='+', dest='layer_sizes', type=int, default=[500], help='Number of nodes in each hidden layer')
    parser.add_argument('-ps', '--pool-size', dest='pool_size', type=int, default=10000, help='How many hard features to collect before merging')
    parser.add_argument('-la', '--lambda', dest='lam', type=float, default=0.2, help='How much does reconstruction cost factor into the merge-increment cost function')
    parser.add_argument('-nm', '--no-merge', dest='no_merge', action='store_true', help='Disable feature merging')
    parser.add_argument('-ni', '--no-increment', dest='no_increment', action='store_true', help='Disable feature increment')
    parser.add_argument('-s', '--scale', dest='scale', type=float, default=1, help='Scaling constant')
    parser.add_argument('-fw', '--filter-width', dest='fw', type=float, default=None, help='Filter width')
    parser.add_argument('-fh', '--filter-height', dest='fh', type=float, default=None, help='Filter height')

    args = parser.parse_args()

    print('forcing 32 bit floats')
    theano.config.floatX = 'float32'
    print(args)
    print('building the model')
    pretrain_model, train_model, test_model, test_model_reconstruction, merge_model, layers = build_model(args)

    # read from raw stdin
    sys.stdin = sys.stdin.detach()
    def get_batch(size):
        count = 0
        batch_x = np.empty((size, args.dimensions), theano.config.floatX)
        batch_y = np.empty(size, np.int32)

        for i in range(size):
            line = sys.stdin.read((args.dimensions + 1) * 4)
            if not line: break

            batch_x[i] = np.frombuffer(line, theano.config.floatX, batch_x.shape[1])
            batch_y[i] = struct.unpack('=f', line[-4:])[0]
            count += 1

        np.resize(batch_x, (count, batch_x.shape[1]))
        np.resize(batch_y, (count,))

        return count, batch_x * args.scale, batch_y

    # pre-training
    print('loading %d examples for pretraining' % args.pretrain_size)
    for i in range(math.ceil(args.pretrain_size / args.pretraining_batch_size)):
        _, x, _ = get_batch(args.pretraining_batch_size)

        print('    pretraining iter %d' % i)
        pretrain_model(x)

    # supervised fine tuning
    lowest_err = float('inf')
    pool_x, pool_y = [], []

    layers[0].dump_filters('start.png', args.fw, args.fh)

    print('supervised finetuning')
    errs = deque()
    reconstruction_costs = deque()

    e1, e2 = 0.01, 0.01
    delta_n = 20
    gamma = 0.5
    merge_round = 0

    for batch in itertools.count():
        size, x, y = get_batch(args.batch_size)
        if size != args.batch_size: break

        # test
        err = np.asscalar(test_model(x, y))

        # determine reconstruction costs
        batch_costs = test_model_reconstruction(x)
        reconstruction_costs.append(np.mean(batch_costs))
        errs.append(err)
        lowest_err = min(err, lowest_err)

        if len(errs) > args.pool_size / args.batch_size:
            errs.popleft()
        if len(reconstruction_costs) > args.pool_size / args.batch_size:
            reconstruction_costs.popleft()

        # collect hard examples
        average_err = np.mean(errs)
        average_cost = np.mean(reconstruction_costs)
        for i, cost in enumerate(batch_costs):
            if cost > average_cost:
                pool_x.append(x[i])
                pool_y.append(y[i])

        print('batch: %4d, errors: %2.3f, average_err (%d): %2.3f lowest_err: %2.3f' % (batch, err, int(args.pool_size / args.batch_size), average_err, lowest_err))

        # merge and increment
        if len(pool_x) > args.pool_size:
            reduction = errs[-1] / (errs[-2] if errs[-2] != 0 else 0.00001)

            if reduction < 1 - e1:
                delta_n += 1
            elif reduction > 1 - e2:
                delta_n //= 2
            delta_m = math.ceil(delta_n * 0.5)

            if args.no_merge:
                delta_m = 0
            if args.no_increment:
                delta_n = 0

            print('    %d merging/incrementing features %d, %d, %f' % (merge_round, delta_m, delta_n, reduction))
            merge_model(np.array(pool_x, dtype=theano.config.floatX), np.array(pool_y, dtype=np.int32), delta_m, delta_n)
            pool_x, pool_y = [], []
            layers[0].dump_filters('%d after.png' % merge_round, args.fw, args.fh)

            merge_round += 1

        # train
        train_model(x, y)

    layers[0].dump_filters('end.png', args.fw, args.fh)

if __name__ == '__main__':
    main()
