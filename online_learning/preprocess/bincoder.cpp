#include <iostream>
#include <vector>

int main() {
    std::cin.sync_with_stdio(false);

    // each input should have 16 variations
    float val;
    while (std::cin >> val) {
        const unsigned char *ptr = reinterpret_cast<const unsigned char *>(&val);
        for (unsigned i = 0; i < sizeof(float); ++i) {
            std::cout << ptr[i];
        }
    }
}
