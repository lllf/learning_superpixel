#include <iostream>
#include <vector>

#include "lodepng.h"

int main() {
    std::vector<unsigned char> png(28 * 28 * 4);

    int counter = 0;
    while (std::cin.good()) {
        float val;
        for (unsigned i = 0; i < 28 * 28; ++i) {
            if (!(std::cin >> val)) {
                return 0;
            }

            png[i * 4 + 0] = static_cast<unsigned char>(val * 255.0f);
            png[i * 4 + 1] = static_cast<unsigned char>(val * 255.0f);
            png[i * 4 + 2] = static_cast<unsigned char>(val * 255.0f);
            png[i * 4 + 3] = 255;
        }

        if (!(std::cin >> val)) {
            return 0;
        }

        std::vector<unsigned char> data;
        lodepng::encode(data, png, 28, 28);
        lodepng::save_file(data, std::to_string(counter++) + ".png");
    }
}
