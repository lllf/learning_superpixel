#!/usr/bin/env python3
import sys, time, struct, os, argparse
import itertools
from collections import deque, Counter

import theano

import numpy as np
import math

def main():
    parser = argparse.ArgumentParser(description='Extract features')
    parser.add_argument('-b', '--batch-size', dest='batch_size', type=int, default=1000, help='Batch size')
    parser.add_argument('-d', '--dimensions', dest='dimensions', type=int, default=28*28, help='Number of dimensions')

    args = parser.parse_args()

    # read from raw stdin
    sys.stdin = sys.stdin.detach()

    def get_batch(size):
        count = 0
        batch_x = np.empty((size, args.dimensions), theano.config.floatX)
        batch_y = np.empty(size, np.int32)

        for i in range(size):
            line = sys.stdin.read((args.dimensions + 1) * 4)
            if not line: break

            batch_x[i] = np.frombuffer(line, theano.config.floatX, batch_x.shape[1])
            batch_y[i] = struct.unpack('=f', line[-4:])[0]
            count += 1

        np.resize(batch_x, (count, batch_x.shape[1]))
        np.resize(batch_y, (count,))

        return count, batch_x, batch_y

    while True:
        count, _, batch_y = get_batch(args.batch_size)

        if count != args.batch_size: break
        counter = Counter(batch_y)
        print(counter)

if __name__ == '__main__':
    main()
